package se331.lab.rest.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Student extends Person {
    String studentId;
    Double gpa;
    String image;
    Integer penAmount;
    String description;
    @ManyToOne
    @ToString.Exclude
    Lecturer advisor;
    @ManyToMany(mappedBy = "students")
    @Builder.Default
    @ToString.Exclude
    List<Course> enrolledCourses = new ArrayList<>();


}
